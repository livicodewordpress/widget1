<?php
error_reporting(E_ALL);
/*
PLugin Name: Owo Twitter Widget
PLugin URI: http:livicode.com
Description: Displays tweetes from twitter 
Version: 1.0
Author: Femi Yale
Author URI: http://owoyale.com
*/

class OwoTwitter extends WP_Widget {


	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {

		parent::__construct(
			'owo_twitter', // Base ID
			__( 'Display Tweets', 'text_domain' ), // Name
			array( 'description' => __( 'A Twitter Widget', 'text_domain' ), ) // Args
		);
	}

	



	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Owoyale Twitter', 'text_domain' );
		$description = ! empty( $instance['description'] ) ? $instance['description'] : __( 'Twitter Decr', 'text_domain' );
		$username = ! empty( $instance['username'] ) ? $instance['username'] : __( 'owoyalefemi', 'text_domain' );
		$tweet_count = ! empty( $instance['tweet_count'] ) ? $instance['tweet_count'] : __( 5, 'text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( esc_attr( 'Title:' ) ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">

		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php _e( esc_attr( 'Description:' ) ); ?></label> 
			<textarea
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" type="text" value="<?php echo esc_attr( $description ); ?>">

		
			
		</textarea>
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php _e( esc_attr( 'Your Username:' ) ); ?></label> 
			<input
				type="text"
				class="widefat"
				id="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" 
				value="<?php echo esc_attr( $username ); ?>"


				/>

		
		</p>


		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'tweet_count' ) ); ?>">
		<?php _e( esc_attr( 'Number of Tweet to retrieve:' ) ); ?></label> 
			<input
				type="number"
				class="widefat"
				style="width:40px"
				id="<?php echo esc_attr( $this->get_field_name( 'tweet_count' ) ); ?>"
				name="<?php echo esc_attr( $this->get_field_name( 'tweet_count' ) ); ?>" 
				min="1"
				max="10"
				value="<?php echo esc_attr( $tweet_count ); ?>"


				/>

		
		</p>
		<?php 
	}


	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract($args);
		extract($instance);
		// print_r($instance);
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
		// echo __( esc_attr( 'Hello, tweets' ), 'text_domain' );
		$data = $this->twitter($tweet_count, $username);
		echo $args['after_widget'];
	}

	private function twitter($tweet_count, $username){
		if (empty($username)) return false;
		$this->fetch_tweets($tweet_count,$username);
	}

	private function fetch_tweets($tweet_count, $username){
		$tweets = wp_remote_get("https://api.twitter.com/1.1/statuses/$username.json");
		$tweets = json_decode($tweets['body']);
		print_r($tweets);
	}




}//end of my widget class




add_action('widgets_init', 'owo_register_twitter');
function owo_register_twitter(){
	register_widget('OwoTwitter' );
}